#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include "ShaderGenerator.h"
#include <stdio.h>
#include <iostream>
#include <vector>


using namespace std;

void recursion(vector<float> &vertices, float *a, float *b, float *c, int recursionDepth);


#if defined(IMGUI_IMPL_OPENGL_LOADER_GL3W)
#include <GL/gl3w.h>    // Initialize with gl3wInit()
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLEW)
#include <GL/glew.h>    // Initialize with glewInit()
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD)
#include <glad/glad.h>
#else
#include IMGUI_IMPL_OPENGL_LOADER_CUSTOM

#endif
#include <GLFW/glfw3.h>

static void glfw_error_callback(int error, const char* description)
{
    fprintf(stderr, "Glfw Error %d: %s\n", error, description);
}


int main(int, char**)
{
    glfwSetErrorCallback(glfw_error_callback);
    if (!glfwInit())
        return 1;

    const char* glsl_version = "#version 430";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);  // 3.2+ only
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);            // 3.0+ only

    GLFWwindow* window = glfwCreateWindow(1280, 720, "PAG 01", NULL, NULL);
    if (window == NULL)
        return 1;

    glfwMakeContextCurrent(window);


#if defined(IMGUI_IMPL_OPENGL_LOADER_GL3W)
    bool err = gl3wInit() != 0;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLEW)
    bool err = glewInit() != GLEW_OK;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD)
    bool err = !gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
#endif
    if (err)
    {
        fprintf(stderr, "Failed to initialize OpenGL loader!\n");
        return 1;
    }


    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO();
    (void)io;

    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init(glsl_version);


    ImGui::StyleColorsClassic();

    bool show_another_window = false;

    ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }



    while (!glfwWindowShouldClose(window))
    {
        glfwPollEvents();

        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        ImGui::Begin("Trojkat Sierpinskiego");

        ImVec2 windowSize = ImVec2(600,400);
        ImGui::SetWindowSize(windowSize);
        int imguiWindowHeight= (int)ImGui::GetWindowHeight();
        int imguiWindowWidth = (int)ImGui::GetWindowWidth();
        glfwWindowHint( GLFW_RESIZABLE, GL_FALSE);
        glfwSetWindowSize(window,imguiWindowWidth,imguiWindowHeight);

        static int recurationDepth = 0;
        ImVec2 buttonSize = ImVec2(50,50);
        ImGui::SliderInt("Liczba iteracji: ",&recurationDepth,0,20);
        ImGui::ColorEdit3("<-Wybór koloru trójkata", (float*)&clear_color);

        if(ImGui::Button("GO!",buttonSize))
        {
            show_another_window = true;
        };

        ImGui::End();

        if (show_another_window)
        {
            GLFWwindow* window2 = glfwCreateWindow(1000, 600, "Trojkat Sierpinskiego", NULL, NULL);
            if (window2 == NULL)
                return 1;
            glfwSwapInterval(1);
            glfwMakeContextCurrent(window2);



            float a[2] = {0.0f,  0.5f };

            float b[2] = {0.5f, -0.5f };

            float c[2] = {-0.5f, -0.5f};

            vector<float> vectorOfCalculatedPoints;

            recursion(vectorOfCalculatedPoints, a, b, c, recurationDepth);

            unsigned int VBO, VAO;

            glGenVertexArrays(1, &VAO);

            glGenBuffers(1, &VBO);

            glBindVertexArray(VAO);

            glBindBuffer(GL_ARRAY_BUFFER, VBO);

            glBufferData(GL_ARRAY_BUFFER, vectorOfCalculatedPoints.size() * sizeof(float), &vectorOfCalculatedPoints.front(), GL_STATIC_DRAW);

            glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), 0);

            glEnableVertexAttribArray(0);


            while (!glfwWindowShouldClose(window2))
            {


                glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

                glClear(GL_COLOR_BUFFER_BIT);

                ShaderGenerator gen;

                int shader = gen.makeShaders();

                glUseProgram(shader);

                int selectedColorLocation = glGetUniformLocation(shader, "selectedColor");

                glUniform4f(selectedColorLocation, clear_color.x, clear_color.y, clear_color.z, clear_color.w);

                glDrawArrays(GL_TRIANGLES , 0, vectorOfCalculatedPoints.size());

                glfwSwapBuffers(window2);

                glfwPollEvents();


            }
            glfwDestroyWindow(window2);
            show_another_window = false;


        }

        ImGui::Render();

        int display_w, display_h;

        glfwMakeContextCurrent(window);

        glfwGetFramebufferSize(window, &display_w, &display_h);

        glViewport(0, 0, display_w, display_h);

        glClearColor(0.0f,0.0f,0.0f,0.0f);

        glClear(GL_COLOR_BUFFER_BIT);

        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

     glfwSwapBuffers(window);
    }

    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}
void recursion(std::vector<float> &vertices, float *a, float *b, float *c, int recursionDepth)
{
    int r = recursionDepth;
    float ab[2], bc[2], ac[2];
    int i;
    if (recursionDepth > 0) {
        for (i = 0; i < 2; i++) ab[i] = ((a[i] + b[i]) / 2);
        for (i = 0; i < 2; i++) bc[i] = ((b[i] + c[i]) / 2);
        for (i = 0; i < 2; i++) ac[i] = ((a[i] + c[i]) / 2);
        recursion(vertices, a, ab, ac, r - 1);
        recursion(vertices, ab, b, bc, r - 1);
        recursion(vertices, ac, bc, c, r - 1);
    }
    else {
        vertices.push_back(a[0]);
        vertices.push_back(a[1]);
        vertices.push_back(b[0]);
        vertices.push_back(b[1]);
        vertices.push_back(c[0]);
        vertices.push_back(c[1]);
    }
}
